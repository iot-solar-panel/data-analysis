import logging
import os

log = logging.getLogger(__name__)

if "DEVICE_LATITUDE" not in os.environ or \
        "DEVICE_LONGITUDE" not in os.environ or \
        "DEVICE_ELEVATION" not in os.environ:
    log.warning("default device position have been set!")

# as default coordinates use Chornobyl Nuclear reactor
latitude = float(os.environ.get("DEVICE_LATITUDE", 51.38953418301934))
longitude = float(os.environ.get("DEVICE_LONGITUDE", 30.099182004169016))
elevation = float(os.environ.get("DEVICE_ELEVATION", 110))

# weather related parameters to account for atmosphere (None for default)
temperature = float(os.environ.get("DEVICE_TEMPERATURE", 14))
pressure = None
