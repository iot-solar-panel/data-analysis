

_motor_voltage = 5
_motor_current = 0.7

_motor_rpm = 5
_motor_degree_per_sec = (360 * _motor_rpm / 60)


def get_motor_consumption(angles):
    time = angles / _motor_degree_per_sec
    return get_motor_time_consumption(time)


def get_motor_time_consumption(time):
    return time * _motor_voltage * _motor_current
