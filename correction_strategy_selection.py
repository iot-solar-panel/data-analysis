import datetime
import itertools
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np

import device_sun_position
from algorithms.minimisation import minimize_golden_ratio
from algorithms.power_integration import integrate_control_scheme
from models.control_scheme import ControlScheme
from models.position_correction import PositionCorrection
from models.sun_power_diagram import SunPowerDiagram
from motor_specs import *


def optimize_correction_positions(scheme: ControlScheme, step: float = 60):
    intervals = list(scheme.get_intervals())

    # case when day is divided by two intervals should be processed as one interval
    if len(intervals) == 2:
        intervals = [(intervals[0][0], intervals[1][1])]
        scheme.corrections.pop(1)

    for (start, end), i in zip(intervals, range(len(scheme.corrections))):
        best_position = get_average_interval_position(start, end, step)
        scheme.corrections[i] = PositionCorrection(start.timestamp, *best_position)

    scheme.corrections[-1] = PositionCorrection(scheme.last.timestamp, *scheme.first.position)

    return scheme


def get_average_interval_position(start, end, step):
    times = np.arange(start.timestamp, end.timestamp, step)
    positions = device_sun_position.sun_position(times)
    return np.average(positions, axis=1)


def get_sun_borders(start_time, end_time, step):
    sunrise_time = start_time
    sunrise = device_sun_position.sun_position(sunrise_time)
    while sunrise[1] < 0:
        sunrise_time += step
        sunrise = device_sun_position.sun_position(sunrise_time)
    sunset_time = end_time
    sunset = device_sun_position.sun_position(sunset_time)
    while sunset[1] < 0:
        sunset_time -= step
        sunset = device_sun_position.sun_position(sunset_time)
    return sunrise_time, sunrise, sunset_time, sunset


def get_date_sun_borders(date, step):
    start_time = datetime.datetime.combine(date, datetime.time(0, 0, 0))
    end_time = start_time + datetime.timedelta(days=1)
    return get_sun_borders(start_time.timestamp(), end_time.timestamp(), step)


def get_total_motor_consumption(scheme: ControlScheme):
    total = 0
    for start, end in scheme.get_all_intervals():
        diff = np.abs(start.position - end.position)
        total += get_motor_consumption(diff[0])
        total += get_motor_consumption(diff[1])
    return total


def maximize_output(func, start, end, tolerance=1e-8):
    def _to_minimise(value):
        return -func(value)

    x_value, func_value = minimize_golden_ratio(_to_minimise, start, end, tolerance)
    return x_value, -func_value


_schemes_by_energy = {}
_schemes_by_interval = {}
_energy_by_interval = {}


def total_power(_correction_interval, start_time, end_time, _delta_t):
    correction_times = np.arange(start_time, end_time, _correction_interval)
    times = itertools.chain(correction_times)

    scheme = ControlScheme(dt, [PositionCorrection(*_t) for _t in
                                zip(times, itertools.repeat(0), itertools.repeat(0))])

    optimize_correction_positions(scheme, _delta_t)

    solar_panel_energy = integrate_control_scheme(scheme, diagram, start_time, end_time, _delta_t)
    motor_consumption = get_total_motor_consumption(scheme)
    total_energy = solar_panel_energy - motor_consumption
    _schemes_by_energy[total_energy] = scheme.copy()
    _schemes_by_interval[_correction_interval] = _schemes_by_energy[total_energy]
    _energy_by_interval[_correction_interval] = [total_energy, solar_panel_energy, motor_consumption]
    return total_energy


def find_best_control_scheme(date, min_interval=None, max_interval=None):
    global _schemes_by_energy, _schemes_by_interval, _energy_by_interval
    _schemes_by_energy = {}
    _schemes_by_interval = {}

    sunrise_time, _, sunset_time, _ = get_date_sun_borders(date, _delta_t)

    min_interval = 1 if min_interval is None else min_interval
    max_interval = datetime.timedelta(days=1).total_seconds() if max_interval is None else max_interval

    def _to_maximize(_interval):
        return total_power(_interval, sunrise_time, sunset_time, _delta_t)

    interval, max_value = maximize_output(_to_maximize, min_interval, max_interval)
    return interval, max_value, _schemes_by_energy[max_value]


def visualize_scheme(scheme, title=None):
    _rise_time, _sunset, _set_time, _sunrise = \
        get_sun_borders(scheme.day_begin_time, scheme.day_end_time, _delta_t)

    _times = np.arange(scheme.day_begin_time, scheme.day_end_time, _delta_t)
    _intermediate = scheme.get_positions(_times)
    _ideal = device_sun_position.sun_position(_times)

    fig, axes = plt.subplots(ncols=2)
    fig.set_size_inches(10, 5)
    axes[0].plot([_times[0], _times[-1]], [_sunset[0], _sunset[0]], c="r")
    axes[0].plot([_times[0], _times[-1]], [_sunrise[0], _sunrise[0]], c="r")
    axes[0].plot(_times, _intermediate[0])
    axes[0].plot(_times, _ideal[0])
    axes[0].set_title(title)

    axes[1].plot([_times[0], _times[-1]], [_sunset[1], _sunset[1]], c="r")
    axes[1].plot([_times[0], _times[-1]], [_sunrise[1], _sunrise[1]], c="r")
    axes[1].plot(_times, _intermediate[1])
    axes[1].plot(_times, _ideal[1])
    axes[1].set_title(title)

    plt.show()
    total_sun_energy = integrate_control_scheme(scheme, diagram, _rise_time, _set_time, _delta_t)
    total_motor_consumption = get_total_motor_consumption(scheme)
    print(f"sun energy: {total_sun_energy}")
    print(f"motor: {total_motor_consumption}")


if __name__ == '__main__':
    data_folder = Path("../data")
    data_paths = sorted(list(data_folder.glob("*.csv")))
    data_path = data_paths[0]
    print(f"processing {data_path}")
    diagram = SunPowerDiagram.from_file(data_path, 5)
    print(f"diagram loaded")

    _delta_t = 60

    dt = datetime.date.fromtimestamp(diagram.time)
    dt = dt.replace(month=1, day=1)
    print(f"processing date: {dt}")

    best_interval, max_value, best_scheme = find_best_control_scheme(dt)
    _positions = best_scheme.get_positions(np.arange(0, 24*3600, _delta_t))
    if np.all(_positions[0, :] == _positions[0, 0]) and \
            np.all(_positions[1, :] == _positions[1, 0]):
        print(f"interval determined - 'infinity'")
    else:
        print(f"interval determined - {best_interval} seconds or {datetime.timedelta(seconds=best_interval)}")

    total_plots = 0

    sorted_schemes = sorted(_schemes_by_interval.items(), key=lambda pair: pair[0], reverse=True)
    for (interval, scheme), _ in zip(sorted_schemes, range(total_plots)):
        visualize_scheme(scheme, title=f"Interval: {interval}")

    visualize_scheme(best_scheme, title=f"Best Interval: {best_interval}")
    # %%
    fig, axes = plt.subplots(ncols=3)
    fig.set_size_inches(15, 5)
    energy_by_interval = dict(sorted(_energy_by_interval.items(), key=lambda pair: pair[0]))
    values = [[key, *value] for key, value in energy_by_interval.items()]
    values = list(zip(*values))

    intervals = values[0]
    axes[0].plot(intervals, values[1])
    axes[0].set_title("Total energy")
    axes[1].plot(intervals, values[2])
    axes[1].set_title("Solar panel energy")
    axes[2].plot(intervals, np.asarray(values[3]) * -1, label="Motor consumption")
    axes[2].set_title("Motor consumption")
    plt.show()

    start_time = datetime.datetime.combine(dt, datetime.time(0, 0, 0))
    end_time = start_time + datetime.timedelta(days=1)

    _rise_time, _, _set_time, _ = get_sun_borders(start_time.timestamp(), end_time.timestamp(), _delta_t)

    power1 = total_power(best_interval, _rise_time, _set_time, 60)
    power2 = total_power(1, _rise_time, _set_time, 1)


    def delta_rise(_v1, _v2):
        return (_v2 - _v1) / _v1 * 100

    print(f"{best_interval:.3f} interval: {power1}\n1 second interval: {power2}\n"
          f"difference: {abs(power1 - power2)}\nrise by: {delta_rise(power1, power2) :.3f} %")

