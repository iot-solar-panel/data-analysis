import copy
import datetime
import itertools
from dataclasses import dataclass, field

import numpy as np

from algorithms.tools import get_intervals, clip_corrections
from .position_correction import PositionCorrection

_DAY_SECONDS = datetime.timedelta(days=1).total_seconds()


@dataclass
class ControlScheme:
    date: datetime.date
    corrections: list[PositionCorrection] = field(default_factory=list)

    @property
    def first(self):
        return self.corrections[0]

    @property
    def last(self):
        return self.corrections[-1]

    @property
    def day_begin_time(self):
        ts = datetime.datetime.combine(self.date, datetime.time(0, 0, 0))
        return ts.timestamp()

    @property
    def day_end_time(self):
        return self.day_begin_time + _DAY_SECONDS

    @classmethod
    def get_uniform(cls, start_time, end_time, positions=None):
        if positions is None:
            positions = itertools.repeat((0, 0))
            amount = 2
        else:
            amount = len(positions)

        uniform_times = np.linspace(start_time, end_time, amount)
        corrections = [PositionCorrection(t, *pos) for t, pos in zip(uniform_times, positions)]
        return cls(datetime.date.fromtimestamp(start_time), corrections)

    def copy(self):
        return ControlScheme(self.date, [corr.copy() for corr in self.corrections])

    def get_intervals(self):
        yield from get_intervals(self.corrections)

    def get_all_intervals(self):
        yield PositionCorrection(self.day_begin_time, *self.last.position), self.first
        yield from self.get_intervals()
        yield self.last, PositionCorrection(self.day_end_time, *self.last.position)

    def cycle_intermediate(self, step):
        cur_time = self.corrections[0].timestamp
        for start, end in self.get_all_intervals():
            while cur_time < end.timestamp:
                yield start.with_time(cur_time)
                cur_time += step

        final = self.corrections[-1]
        last_time = self.day_end_time
        while cur_time < last_time:
            yield final.with_time(cur_time)
            cur_time += step

    def insert_borders(self, corrections: list[PositionCorrection]):
        corr = copy.deepcopy(corrections)
        insert_pos = 0
        for border in self.corrections:
            if corr[insert_pos].timestamp > border.timestamp:
                corr.insert(insert_pos, border.copy())
            else:
                insert_pos += 1
        return corr

    def clipped_intermediate(self, step, end_time=None):
        if end_time is None:
            yield from self.cycle_intermediate(step)
            return

        intervals = get_intervals(clip_corrections(self.corrections, end_time))
        cur_time = self.corrections[0].timestamp
        for start, end in intervals:
            while cur_time < end.timestamp:
                yield start.with_time(cur_time)
                cur_time += step

            if cur_time != end.timestamp:
                yield start.with_time(end.timestamp)

    def get_positions(self, times):
        positions = np.zeros((2, len(times)))
        cur_time_i = 0
        try:
            it_time = iter(times)
            cur_time = next(it_time)
            for start, end in self.get_all_intervals():
                cur_position = start.position
                while start.timestamp <= cur_time <= end.timestamp:
                    positions[:, cur_time_i] = cur_position
                    cur_time_i += 1
                    cur_time = next(it_time)
        finally:
            return positions
