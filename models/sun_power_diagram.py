import datetime
from pathlib import Path
from typing import Union

import numpy as np
import pandas as pd

from device_sun_position import sun_position
from .directional_diagram import DirectionalDiagram


class SunPowerDiagram:

    def __init__(self, diagram: DirectionalDiagram, time):
        self.diagram = diagram
        self._time = time

        # we assume that maximum intensity corresponds to sun position
        self._diagram_max_pos = np.atleast_2d(diagram.get_power_argmax())
        self._shift_pos = np.zeros((2, 1))
        self.set_time(time)
        self.get_position_powers = np.vectorize(self.get_timed_power)

    @property
    def min_position(self):
        return np.asarray([self.diagram.min_azimuth, self.diagram.min_altitude]) \
               + self._shift_pos

    @property
    def max_position(self):
        return np.asarray([self.diagram.max_azimuth, self.diagram.max_altitude]) \
               + self._shift_pos

    @property
    def az_range(self):
        for val in self.diagram.az_range:
            yield val + self._shift_pos[0]

    @property
    def al_range(self):
        for val in self.diagram.al_range:
            yield val + self._shift_pos[1]

    @classmethod
    def from_table(cls, table: pd.DataFrame, angle_step):
        return cls(DirectionalDiagram.from_table(table, angle_step), table.time[0])

    @property
    def time(self):
        return self._time

    def set_time(self, value):
        sun_pos = np.asarray(sun_position(value))
        self._shift_pos = self._diagram_max_pos - sun_pos.transpose()

    def get_max_power_position(self):
        position, power = self.diagram.get_max_position()
        return position + self._shift_pos

    @property
    def measurement_date(self):
        return datetime.date.fromtimestamp(self._time)

    def get_timed_power(self, time, position):
        sun_pos = np.atleast_2d(sun_position(time))
        shifts = np.empty(sun_pos.shape)
        shifts[0, :] = self._diagram_max_pos[:, 0]
        shifts[1, :] = self._diagram_max_pos[:, 1]
        shifts[0, :] -= sun_pos[0, :]
        shifts[1, :] -= sun_pos[1, :]
        return self.diagram.get_power(position + shifts)

    @classmethod
    def from_file(cls, file_path: Union[str, Path], angle_step):
        if not isinstance(file_path, Path):
            file_path = Path(file_path)

        with file_path.open("r") as f:
            table = pd.read_csv(f)
            return cls.from_table(table, angle_step)
