import numpy as np
import pandas as pd
import scipy.interpolate as interpolate


class DirectionalDiagram:
    """interpolated version of directional power diagram"""

    def __init__(self, az_range: list[float], al_range: list[float], step: float, power: np.ndarray):
        self.min_azimuth = min(az_range)
        self.max_azimuth = max(az_range)
        self.min_altitude = min(al_range)
        self.max_altitude = max(al_range)
        self.step = step

        az_v, al_v = np.meshgrid(az_range, al_range)
        self._interpolated_power = interpolate.LinearNDInterpolator(list(zip(az_v.flatten(), al_v.flatten())),
                                                                    power.flatten(), fill_value=0)
        self._power_table = power

    @property
    def power_table(self):
        return self._power_table

    @property
    def az_range(self):
        yield from self._gen_values(self.min_azimuth, self.max_azimuth)

    @property
    def al_range(self):
        yield from self._gen_values(self.min_altitude, self.max_altitude)

    def _gen_values(self, start, end):
        current = start
        while current < end:
            yield current
            current += self.step

    @classmethod
    def from_table(cls, table: pd.DataFrame, angle_step):
        min_az, min_al = table.azimuth.min(), table.altitude.min()
        az_range = list(range(min_az, table.azimuth.max() + 1, angle_step))
        al_range = list(range(min_al, table.altitude.max() + 1, angle_step))

        averages_l = [[list() for _ in range(len(al_range))]
                      for _ in range(len(az_range))]

        def get_indices(_az, _al):
            return (_az - min_az) // angle_step, \
                   (_al - min_al) // angle_step

        for _, az, al, _, power in table.itertuples(index=False):
            az_i, al_i = get_indices(az, al)
            averages_l[az_i][al_i].append(power)

        averages = np.zeros((len(al_range), len(az_range)), dtype=np.float64)
        for az_i in range(len(az_range)):
            for al_i in range(len(al_range)):
                values = averages_l[az_i][al_i]
                averages[al_i, az_i] = np.average(values) if len(values) > 0 else 0

        return cls(az_range, al_range, angle_step, averages)

    def get_power(self, pos):
        pos = pos % 360
        power: np.ndarray = self._interpolated_power(pos[0], pos[1])
        if power.ndim == 0:
            return float(power)
        return power

    def get_power_max(self) -> (np.ndarray, np.float64):
        return self.get_max_position()[1]

    def get_power_argmax(self):
        return self.get_max_position()[0]

    def get_max_position(self):
        power = self.power_table
        max_i_al, max_i_az = np.unravel_index(np.argmax(power), power.shape)
        return np.asarray([max_i_az * self.step + self.min_azimuth,
                           max_i_al * self.step + self.min_altitude],
                          dtype=np.float64), power[max_i_al, max_i_az]
