import copy
import datetime
from dataclasses import dataclass

import numpy as np


@dataclass
class PositionCorrection:
    timestamp: float
    azimuth: float
    altitude: float

    def __str__(self):
        dt = datetime.datetime.fromtimestamp(self.timestamp)
        return f"PositionCorrection({dt}, az={self.azimuth}, al={self.altitude})"
    
    @property
    def position(self):
        return np.asarray((self.azimuth, self.altitude), dtype=np.float64)

    def copy(self):
        return copy.copy(self)

    def with_time(self, time):
        cp = self.copy()
        cp.timestamp = time
        return cp

    def get_corrections(self, start_time, end_time, step):
        time = start_time
        while time < end_time:
            new_corr = self.copy()
            new_corr.timestamp = time
            yield new_corr
            time += step
