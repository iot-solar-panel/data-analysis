import datetime
import unittest

import numpy as np

import device_sun_position
from models.directional_diagram import DirectionalDiagram
from models.sun_power_diagram import SunPowerDiagram


class TestDiagramOperations(unittest.TestCase):

    def setUp(self) -> None:
        self.sun_diagram = SunPowerDiagram.from_file("test_data.csv", 5)
        self.measure_time = datetime.datetime.fromtimestamp(self.sun_diagram.time)

    def test_diagram_max(self):
        max_power = np.max(self.sun_diagram.diagram.power_table)
        sun_pos = device_sun_position.sun_position(self.sun_diagram.time)
        self.assertAlmostEqual(float(self.sun_diagram.get_timed_power(self.sun_diagram.time, sun_pos)), max_power)

    def test_diagram_max_move(self):
        max_power = np.max(self.sun_diagram.diagram.power_table)

        time = self.sun_diagram.time
        sun_pos = device_sun_position.sun_position(time)
        self.assertAlmostEqual(self.sun_diagram.get_timed_power(time, sun_pos), max_power)

        time = self.sun_diagram.time + 3600
        new_sun_pos = device_sun_position.sun_position(time)
        self.assertTrue(np.any(sun_pos != new_sun_pos))
        self.assertAlmostEqual(self.sun_diagram.get_timed_power(time, new_sun_pos), max_power)

    def test_sun_diagram_zero_when_hidden(self):
        midnight_time = self.measure_time.replace(hour=0, minute=0, second=0)
        self.assertAlmostEqual(0, self.sun_diagram.get_timed_power(midnight_time, np.array([[280, 35]]).transpose()))

    def test_sun_diagram_not_zero_at_noon(self):
        midday_time = self.measure_time.replace(hour=12, minute=0, second=0)
        self.assertTrue(self.sun_diagram.get_timed_power(midday_time, np.array([[280, 35]]).transpose()) > 0)

    def test_diagram_timed_power(self):
        time = self.sun_diagram.time + 1000
        pos = np.array([[280, 35]]).transpose()
        timed_power = self.sun_diagram.get_timed_power(time, pos)

        self.assertAlmostEqual(timed_power, self.sun_diagram.get_timed_power(time, pos))

    def test_tracking_sun_trajectory_always_max_power(self):
        start = self.measure_time.replace(hour=10, minute=0, second=0).timestamp()
        end = self.measure_time.replace(hour=18, minute=0, second=0).timestamp()
        times = np.arange(start, end, 60)

        max_power = self.sun_diagram.diagram.get_power_max()
        sun_positions = device_sun_position.sun_position(times)
        real_power = self.sun_diagram.get_timed_power(times, sun_positions)

        self.assertTrue(np.all(np.abs(real_power - max_power) < 1e-12))


class TestDiagramValid(unittest.TestCase):

    def setUp(self) -> None:
        def f(x, y):
            return 12 * np.cos((np.power(x, 2) + np.power(y, 2)) / 4) \
                   / (3 + np.power(x, 2) + np.power(y, 2))

        step = 10
        az_range = list(range(0, 360, step))
        al_range = list(range(0, 91, step))

        self.az_v, self.al_v = np.meshgrid(az_range, al_range)
        self.values = f(self.az_v, self.al_v)

        self.diagram = DirectionalDiagram(az_range, al_range, step, self.values)

    def test_diagram_valid(self):
        for i in range(self.az_v.shape[0]):
            for j in range(self.az_v.shape[1]):
                power = self.diagram.get_power(np.array((self.az_v[i, j], self.al_v[i, j])))
                self.assertAlmostEqual(power, self.values[i, j])

    def test_power_tables_equal(self):
        def _assert(actual, expected):
            self.assertAlmostEqual(actual, expected)

        _assert_v = np.vectorize(_assert)

        power_table = self.diagram.power_table
        _assert_v(power_table, self.values)
