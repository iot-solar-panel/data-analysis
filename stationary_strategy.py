from datetime import datetime

import numpy as np

from models.position_correction import PositionCorrection
from models.sun_power_diagram import SunPowerDiagram
from algorithms.power_integration import integrate_control_scheme


sun_diagram = SunPowerDiagram.from_file("../data/clouds1.csv", 5)

measure_time = datetime.fromtimestamp(sun_diagram.time)
start_time = measure_time.replace(hour=10, minute=0, second=0).timestamp()
end_time = measure_time.replace(hour=20, minute=0, second=0).timestamp()

stationary_total_energy = []

al_range = list(sun_diagram.diagram.al_range)
az_range = list(sun_diagram.diagram.az_range)
is_azimuth_bigger = len(az_range) > len(al_range)
big_range, small_range = \
    (az_range, al_range) \
    if is_azimuth_bigger else \
    (al_range, az_range)

for big in big_range:
    dim_fixed_total = []
    for small in small_range:
        if is_azimuth_bigger:
            az, al = big, small
        else:
            az, al = small, big

        corrections = [PositionCorrection(start_time, az, al),
                       PositionCorrection(end_time, az, al)]
        power = integrate_control_scheme(corrections, sun_diagram, end_time)
        dim_fixed_total.append(power)

    max_pow_i = np.argmax(dim_fixed_total)
    max_pow = dim_fixed_total[max_pow_i]
    max_small = small_range[max_pow_i]

    what = "azimuth" if is_azimuth_bigger else "altitude"
    print(f"{what} processed: {big}, max_at {max_small} = {max_pow}")
    stationary_total_energy.append(dim_fixed_total)

stationary_total_energy = np.asarray(stationary_total_energy)
print(stationary_total_energy)
