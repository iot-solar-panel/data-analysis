import os

import numpy as np

import device_position_params as specs

# os.environ.setdefault("NUMBA_DISABLE_JIT", "1")
from sun_position.sunposition import sunpos


def _sun_position(timestamp):
    azimuth, zenith, _, _, _ = sunpos(timestamp, specs.latitude, specs.longitude, specs.elevation,
                                      specs.temperature, specs.pressure)
    altitude = 90 - zenith
    return np.float64(azimuth), np.float64(altitude)


_sun_position_v = np.vectorize(_sun_position)


def sun_position(timestamp):
    timestamp = np.atleast_1d(timestamp)
    return _sun_position_v(timestamp)


if __name__ == '__main__':
    print(sun_position(1653059937.2305858))

