import copy
from typing import Iterable


def clip_corrections(corrections, end_time):
    corrections = copy.deepcopy(corrections)
    if end_time is not None:
        while end_time <= corrections[-1].timestamp:
            corrections.pop(-1)
        corrections.append(corrections[-1].with_time(end_time))
    return corrections


def get_intervals(values: Iterable):
    first_it = iter(values)
    second_it = iter(values)
    next(second_it)

    for start, end in zip(first_it, second_it):
        yield start, end
