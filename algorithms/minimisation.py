def minimize_golden_ratio(func, a, b, epsilon):
    size = b - a
    x1 = a + 0.382 * size
    x2 = a + 0.618 * size
    f1 = func(x1)
    f2 = func(x2)

    while size > epsilon:
        if f1 > f2:
            a = x1
            size = b - a
            x1 = x2
            f1 = f2
            x2 = a + 0.618 * size
            f2 = func(x2)
        else:
            b = x2
            size = b - a
            x2 = x1
            f2 = f1
            x1 = a + 0.382 * size
            f1 = func(x1)

    result = (a + b) / 2
    return result, func(result)
