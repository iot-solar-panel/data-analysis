import numpy as np

from models.control_scheme import ControlScheme
from models.sun_power_diagram import SunPowerDiagram


def integrate_control_scheme(scheme: ControlScheme,
                             power_diagram: SunPowerDiagram,
                             start_time, end_time=None, step=60):
    if end_time is None:
        end_time = scheme.day_end_time

    times = np.concatenate((np.arange(start_time, end_time, step), np.atleast_1d(np.asarray([end_time]))))
    positions = scheme.get_positions(times)
    return integrate_sun_diagram(power_diagram, times, positions)


def integrate_sun_diagram(power_diagram: SunPowerDiagram, times, positions):
    power_values = power_diagram.get_timed_power(times, positions)
    return np.trapz(power_values, times)
